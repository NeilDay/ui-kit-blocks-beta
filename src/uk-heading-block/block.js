import map from 'lodash/map';

import classnames from 'classnames';

import hexToRGBA from '../common-inspector-controls/hex-to-rgba';
import marginClasses from '../common-functions/margin';
import HorizontalAlignmentButtons from '../common-inspector-controls/horizontal_align';


//  Import CSS.
import './style.scss';
import './editor.scss';

// Import icon
import icon from '../icon';

import icons from '../icons';

const { __ } = wp.i18n; 
const { registerBlockType } = wp.blocks; 

const {
	setAttributes,
	Fragment,
} = wp.element;

const { 
	Toolbar,
	RichText,
	InspectorControls, 
} = wp.blockEditor;

const {
	SelectControl,
    PanelBody,
    PanelRow,
    Dashicon
} = wp.components;

const ukHeadingTagOptions = [

	{label : 'h1', 	value: 'h1'},
	{label : 'h2',	value: 'h2'},
	{label : 'h3', 	value: 'h3'},
	{label : 'h4',	value: 'h4'},
	{label : 'h5',	value: 'h5'},
	{label : 'h6',	value: 'h6'},
]

const ukHeadingStyleOptions = [

	{label : 'default', 	value: 'uk-heading'},
	{label : 'divider', 	value: 'uk-heading-divider'},
	{label : 'bullet', 		value: 'uk-heading-bullet'},
	{label : 'line', 		value: 'uk-heading-line'},
]

const ukHeadingSizeOptions = [

	{label : 'default', 	value: ''},
	{label : 'small', 		value: 'uk-heading-small'},
	{label : 'medium', 		value: 'uk-heading-medium'},
	{label : 'large', 		value: 'uk-heading-large'},
	{label : 'xlarge', 		value: 'uk-heading-xlarge'},
	{label : '2xlarge', 	value: 'uk-heading-2xlarge'},
]


registerBlockType( 'uk-gb/uk-heading', {

	title: __( 'UI Kit Heading' ), // Block title.
	icon, 								// Block icon
	category: 'uk-gb-blocks', 				// Block category
	keywords: [ 						// Search by these keywords
		__( 'uk', 'UK', 'uikit' ),
	],
	supports: {
		className: true,
	},

	attributes: {
		
		content: {
			type: 'string',
			default: 'Heading'
		},
		tagName: {
			type: 'string',
			default: 'h2'
		},
		style: {
			type: 'string',
			default: ''
		},
		size: {
			type: 'string',
			default: ''
		},
		horizontalAlignment: {
			type: 'string',
			default: 'uk-text-left'
		}
		
	},

	edit: function( props ) {


		// Setup the attributes.
		const {
			attributes: {
				content,
				tagName,
				style,
				size,
				horizontalAlignment
			},
			isSelected,
			className,
			setAttributes
		} = props;

		var ukMarginClasses = marginClasses(props.attributes);

		const deskHalignItems = [ 
	  		{ id: 1, value: 'uk-text-left', icon:<Dashicon icon="align-left" />},
  			{ id: 2, value: 'uk-text-center', icon:<Dashicon icon="align-center" />},
  			{ id: 3, value: 'uk-text-right', icon:<Dashicon icon="align-right" />}
	  	];
		
		var ukHeadingWrapClasses = classnames({
  			[ukMarginClasses] : ukMarginClasses,
		});

		var ukHeaderClasses = classnames({
  			'uk-divider-icon': props.attributes.icon,
  			'uk-divider-small': !props.attributes.icon && props.attributes.small,
		});
		
		const UkHeadingTag = `${props.attributes.tagName}`;		

		const UkHeadingSpan  = () => {

			if (props.attributes.style === 'uk-heading-line') {

				return (
					
					<span></span>
				)

			} else {
				return null
			}
		}

		var textShadowAttributes = [
			'ukgb_TextShadow',
			'ukgb_TextShadowX',
			'ukgb_TextShadowy',
			'ukgb_TextShadowBlur',
			'ukgb_TextShadowColor',
			'ukgb_TextShadowOpacity',
		]

		const hasTextShadowAttribute = textShadowAttributes.some(attributeName => props.attributes[attributeName]);

		
		

		if (props.attributes.ukgb_TextShadow !== false ){
			var backgroundRGBA = hexToRGBA( props.attributes.ukgb_TextShadowColor, props.attributes.ukgb_TextShadowOpacity )
			var actualShadow = props.attributes.ukgb_TextShadowX+'px '+props.attributes.ukgb_TextShadowY+'px '+props.attributes.ukgb_TextShadowBlur+'px '+backgroundRGBA
		} else {
			var actualShadow = null;
		}
		
		return [
			<InspectorControls>
					<PanelBody
						title={ __( 'UI Kit Heading Options' ) }
						initialOpen={ true }
					>	
						<SelectControl
							label={ __( 'Heading Tag' ) }
							value={ props.attributes.ukHeadingTagOptions }
							options={ ukHeadingTagOptions }
							onChange={ ( value ) => {
								props.setAttributes( {
									tagName: value,
								} );
							} }
						/>
						<SelectControl
							label={ __( 'Heading Style' ) }
							value={ props.attributes.style }
							options={ ukHeadingStyleOptions }
							onChange={ ( value ) => {
								props.setAttributes( {
									style: value,
								} );
							} }
						/>
						<SelectControl
							label={ __( 'Heading Size' ) }
							value={ props.attributes.size }
							options={ ukHeadingSizeOptions }
							onChange={ ( value ) => {
								props.setAttributes( {
									size: value,
								} );
							} }
						/>
						<h2>Horizontal Alignment</h2>
						<div>
							<HorizontalAlignmentButtons
								horizontalAlignment={props.attributes.horizontalAlignment}
								items={deskHalignItems}
								onClick={ ( value ) => props.setAttributes({ horizontalAlignment: value })}
							/>
						</div>
					</PanelBody>
				</InspectorControls>,
			<Fragment>
				<div class={`hello-sahdow ${ukHeadingWrapClasses}`}>
					<UkHeadingTag
						className={ classnames( {
								[ props.attributes.size ]: props.attributes.size,
								[ props.attributes.style ]: props.attributes.style,
								[ props.attributes.horizontalAlignment ]: props.attributes.horizontalAlignment
							} ) }>
						<span>
							<RichText
								identifier="content"
								value={ props.attributes.content }
								onChange={ ( value ) => props.setAttributes( { content: value } ) }
								placeholder={ props.attributes.content || __( 'Heading...' ) }
								style={
									{
										color: props.attributes.ukgb_TextColor,
			  							textShadow: actualShadow,
									}
								}
							/>	
						</span>
					</UkHeadingTag>
				</div>
					
			</Fragment>
			
		];
	},

	save: function( props ) {

		const {
			attributes: {
				content,
				tagName,
				style,
				size,
				horizontalAlignment
			},
		} = props;

		var ukMarginClasses = marginClasses(props.attributes);

		const deskHalignItems = [ 
	  		{ id: 1, value: 'uk-text-left', icon:<Dashicon icon="align-left" />},
  			{ id: 2, value: 'uk-text-center', icon:<Dashicon icon="align-center" />},
  			{ id: 3, value: 'uk-text-right', icon:<Dashicon icon="align-right" />}
	  	];
		
		var ukDividerWrapClasses = classnames({
  			[ukMarginClasses] : ukMarginClasses,
  			
		});

		var ukDividerClasses = classnames({
  			'uk-divider-icon': props.attributes.icon,
  			'uk-divider-small': !props.attributes.icon && props.attributes.small,
		});

		const UkHeadingTag = `${props.attributes.tagName}`;		

		const UkHeadingSpan  = () => {

			if (props.attributes.style === 'uk-heading-line') {

				return (
					
					<span></span>
				)

			} else {
				return null
			}
		}

		return (
			<UkHeadingTag
				className={`uk-gb-block ${classnames(ukDividerWrapClasses, {
						[ props.attributes.size ]: props.attributes.size,
						[ props.attributes.style ]: props.attributes.style,
						[props.attributes.horizontalAlignment] : props.attributes.horizontalAlignment
					} )}` }>
				<span>
					<RichText.Content
						value={ props.attributes.content }
					/>
				</span>
			</UkHeadingTag>

		);

	},

} );
