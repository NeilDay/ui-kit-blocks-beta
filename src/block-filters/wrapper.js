import React from 'react';

import map from 'lodash/map';

import icons from '../icons';

import classnames from 'classnames';

const { __ } = wp.i18n; // Import __() from wp.i18n

const {
	createElement,
	forwardRef,
	setAttributes,
} = wp.element;

// Import Inspector components
const {
} = wp.components;

export function UkWrapper( {
	attributes,
	children,
	doWidth

} ) {
	
	//Inner absolutely positioned div for colour/gradient overlay
	const InnerBG = () => {

		var innerBG = false

		if (attributes.bg_final) {

			return (
				
				<div className="overlay-color uk-fill-parent"
					style={ {background:attributes.bg_final} }
				>

				</div>
					
				
				
			)

		} else {
			return null
		}
	}
	
	//Empty style
	var style = {};

	//If we have image add it to style
	if (attributes.background_image) {

		style = {backgroundImage: `url(${attributes.background_image})`}
	}
	
	// Width classes
	const ukWidthClasses = classnames({
		'alignfull'	: attributes.aligm === 'full',
		[attributes.ukDeskWidth] : attributes.ukDeskWidth,
		[attributes.ukTabWidth] : attributes.ukTabWidth,
		[attributes.ukMobWidth] : attributes.ukMobWidth
	})

	// Width classes
	const ukPaddingClasses = classnames({

		[attributes.ukPadding] : attributes.ukPadding,
		'uk-padding-remove' : attributes.ukPaddingRemove,
		'uk-padding-remove-top' : attributes.ukPaddingRemoveTop,
		'uk-padding-remove-bottom' : attributes.ukPaddingRemoveBottom,
		'uk-padding-remove-left' : attributes.ukPaddingRemoveLeft,
		'uk-padding-remove-right' : attributes.ukPaddingRemoveRight,
		'uk-padding-remove-vertical' : attributes.ukPaddingRemoveVertical,
		'uk-padding-remove-horizontal' : attributes.ukPaddingRemoveHorizontal,
	})
	
	// Atts required for bg
	const backgroundttributes = [
		'background_image',
        'backgroundColor_1'
	] 
	const hasBackgroundAttribute = backgroundttributes.some(attributeName => attributes[attributeName]);

	// If we have bg atts - add the bg + width
	if (hasBackgroundAttribute) {
		return (
			
	   		<div 
	   			className={ `uk-gb-wrapper ${ukPaddingClasses} uk-position-relative ${attributes.background_image ? 'uk-background-cover' : ''} ${doWidth === true ? ukWidthClasses : ''}`}
	   			{...attributes.has_parallax && {'uk-parallax' : 'bgy: -200'}}
	   			style={style}
	   			>
	   			<InnerBG/>
	   			{children}			
	   		</div>

	   	);

	} else {
		//Just add the non bg atts - width , animation etc.
		return (
			
	   		<div 
	   			className={ `wp-block uk-position-relative ${ukPaddingClasses} ${doWidth === true ? ukWidthClasses : ''}`}>
	   			{children}
	   		</div>

	   	);

	} 

	   	
}

export default forwardRef( UkWrapper );