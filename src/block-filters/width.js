import classnames from 'classnames';

import UkWidthControl from '../common-inspector-controls/width';

import assign from 'lodash/assign';


//WP Dependencies
const { 
	createHigherOrderComponent 
} = wp.compose;

const {
	setAttributes,
} = wp.element;

const { 
	Fragment 
} = wp.element;

const { 
	InspectorControls 
} = wp.blockEditor;

const { 
	PanelBody, 
	SelectControl 
} = wp.components;

const { 
	addFilter 
} = wp.hooks;

const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const enableUkWidthOnBlocks = [
	'uk-gb/uk-section-block',
	'uk-gb/uk-grid-item',
	'uk-gb/uk-item-block',
	'uk-gb/uk-split-item',
];


// ADD ATTRIBUTES TO BLOCKS //
//////////////////////////////

const addUkWidthAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkWidthOnBlocks.includes( name ) ) {
		return settings;
	}

	// this is a Grid Item set specific widths
	if (name === 'uk-gb/uk-grid-item') {
		// Use Lodash's assign to gracefully handle if attributes are undefined
		settings.attributes = Object.assign( settings.attributes, {
			ukDeskWidth: {
				type: 'string',
				default: 'uk-width-1-2@m'
			},
			ukTabWidth: {
				type: 'string',
				default: 'uk-width-1-2@s'
			},
			ukMobWidth: {
				type: 'string',
				default: 'uk-width-1-1'
			},
		} );

	} else {
		//Set null widths
		// Use Lodash's assign to gracefully handle if attributes are undefined
		settings.attributes = Object.assign( settings.attributes, {
			ukDeskWidth: {
				type: 'string',
				default: null
			},
			ukTabWidth: {
				type: 'string',
				default: null
			},
			ukMobWidth: {
				type: 'string',
				default: null
			},
		} );
	}

	return settings;
};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/uk-width', addUkWidthAttribute );

// ADD WIDTH CONTROLS TO INSPECTOR //
/////////////////////////////////////

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const ukWidthInspector = createHigherOrderComponent( ( BlockEdit ) => {

		

	return ( props ) => {
		var updateAttribute = (key,value) => {

			props.setAttributes({ [key]: value });

		}

		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkWidthOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 

			ukDeskWidth,
			ukTabWidth,
			ukMobWidth

		} = props.attributes;

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<UkWidthControl
						ukDeskWidth={props.attributes.ukDeskWidth}
						ukTabWidth={props.attributes.ukTabWidth}
						ukMobWidth={props.attributes.ukMobWidth}
						onChange = {updateAttribute}
					>
					</UkWidthControl>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'ukWidthInspector' );

addFilter( 'editor.BlockEdit', 'uk-gb/uk-width-control', ukWidthInspector );


const editorWidthClasses = createHigherOrderComponent((BlockListBlock) => {
  	return props => {
  		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkWidthOnBlocks.includes( props.name ) ) {
			return <BlockListBlock { ...props } />
		}
		
		const { 
			align,
			ukDeskWidth,
			ukTabWidth,
			ukMobWidth

		} = props.attributes;

		// Atts required for bg

		if (props.attributes.align === 'full') {
			
			var widthAttributes = [
				'nowidth',
				'alignfull'
			] 

		} else if (props.attributes.align === 'wide') {
			
			var widthAttributes = [
				'nowidth',
				'alignwide'
			] 

		} else {
			
			var widthAttributes = [
				'ukDeskWidth',
				'ukTabWidth',
				'ukMobWidth'
			] 

		}	

		const hasWidthAttribute = widthAttributes.some(attributeName => props.attributes[attributeName]);

		// If we have bg atts - add the bg + width
		if (hasWidthAttribute) {
			// Width classes
			const ukWidthClasses = classnames('has-uk-width', props.className, {
				[props.attributes.ukDeskWidth] : props.attributes.ukDeskWidth,
				[props.attributes.ukTabWidth] : props.attributes.ukTabWidth,
				[props.attributes.ukMobWidth] : props.attributes.ukMobWidth
			})

    		return <BlockListBlock { ...props } className={ukWidthClasses} />

    	} else {
			
			return <BlockListBlock { ...props } />

    	}
  	}
}, 'editorWidthClasses')
wp.hooks.addFilter('editor.BlockListBlock', 'my-plugin/with-custom-class-name', editorWidthClasses)

function addUkWidthSave( extraProps, blockType, attributes ) {

	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkWidthOnBlocks.includes( blockType.name ) ) {
		return extraProps;
	}

	if (attributes.align === 'full') {
		var widthAttributes = [
			'nowidth',
			'alignfull'
		] 

	} else if (attributes.align === 'wide') {
		var widthAttributes = [
			'nowidth',
			'alignwide'
		] 

	} else {
		var widthAttributes = [
			'ukDeskWidth',
			'ukTabWidth',
			'ukMobWidth'
		] 

	}	

	const hasWidthAttribute = widthAttributes.some(attributeName => attributes[attributeName]);

	// If we have bg atts - add the bg + width
	if (hasWidthAttribute) {
		// Width classes
		const ukWidthClasses = classnames(extraProps.className, {
			[attributes.ukDeskWidth] : attributes.ukDeskWidth,
			[attributes.ukTabWidth] : attributes.ukTabWidth,
			[attributes.ukMobWidth] : attributes.ukMobWidth
		})

		return lodash.assign( extraProps, { className: ukWidthClasses } );

	} else {
		
		return extraProps

	}

}
 
wp.hooks.addFilter('blocks.getSaveContent.extraProps','my-plugin/add-background-color-style',addUkWidthSave);


