import classnames from 'classnames';

import UkPaddingControl from '../common-inspector-controls/padding';


//WP Dependencies
const { 
	createHigherOrderComponent 
} = wp.compose;

const {
	setAttributes,
} = wp.element;

const { 
	Fragment 
} = wp.element;

const { 
	InspectorControls 
} = wp.blockEditor;

const { 
	PanelBody, 
	SelectControl 
} = wp.components;

const { 
	addFilter 
} = wp.hooks;

const { __ } = wp.i18n;

// Enable spacing control on the following blocks
const enableUkPaddingOnBlocks = [
	'uk-gb/uk-grid-block',
	'uk-gb/uk-item-block',
	'uk-gb/uk-grid-item',
	'uk-gb/uk-split-item-left',
	'uk-gb/uk-split-item-right',
	'uk-gb/uk-split-item',
];


// ADD ATTRIBUTES TO BLOCKS //
//////////////////////////////

const addUkPaddingAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkPaddingOnBlocks.includes( name ) ) {
		return settings;
	}

	// this is a Grid Item set specific widths
	if (name === 'uk-gb/uk-section-block') {
		// Use Lodash's assign to gracefully handle if attributes are undefined
		settings.attributes = Object.assign( settings.attributes, {
			ukPadding: {
				type: 'string',
				default: 'large'
			},
			ukPaddingRemove: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveTop: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveBottom: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveLeft: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveRight: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveVertical: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveHorizontal: {
				type: 'bool',
				default: true
			}
		} );

	} else {
		//Set null widths
		// Use Lodash's assign to gracefully handle if attributes are undefined
		settings.attributes = Object.assign( settings.attributes, {
			ukPadding: {
				type: 'string',
				default: 'none'
			},
			ukPaddingRemove: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveTop: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveBottom: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveLeft: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveRight: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveVertical: {
				type: 'bool',
				default: false
			},
			ukPaddingRemoveHorizontal: {
				type: 'bool',
				default: false
			}
		} );
	}

	return settings;
};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/uk-padding', addUkPaddingAttribute );

// ADD WIDTH CONTROLS TO INSPECTOR //
/////////////////////////////////////

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const ukPaddingInspector = createHigherOrderComponent( ( BlockEdit ) => {

		

	return ( props ) => {
		var updateAttribute = (key,value) => {

			props.setAttributes({ [key]: value });

		}

		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkPaddingOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 

			ukPadding,
			ukPaddingRemove,
			ukPaddingRemoveTop,
			ukPaddingRemoveBottom,
			ukPaddingRemoveLeft,
			ukPaddingRemoveRight,
			ukPaddingRemoveVertical,
			ukPaddingRemoveHorizontal,

		} = props.attributes;

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<UkPaddingControl

						ukPadding={props.attributes.ukPadding}
						ukPaddingRemove={props.attributes.ukPaddingRemove}
						ukPaddingRemoveTop={props.attributes.ukPaddingRemoveTop}
						ukPaddingRemoveBottom={props.attributes.ukPaddingRemoveBottom}
						ukPaddingRemoveLeft={props.attributes.ukPaddingRemoveLeft}
						ukPaddingRemoveRight={props.attributes.ukPaddingRemoveRight}
						ukPaddingRemoveLeft={props.attributes.ukPaddingRemoveLeft}
						ukPaddingRemoveVertical={props.attributes.ukPaddingRemoveVertical}
						ukPaddingRemoveHorizontal={props.attributes.ukPaddingRemoveHorizontal}
						onChange = {updateAttribute}

					>
					</UkPaddingControl>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'ukPaddingInspector' );

addFilter( 'editor.BlockEdit', 'uk-gb/uk-padding-control', ukPaddingInspector );

