import get from 'lodash/get';
import map from 'lodash/map';

import icons from '../icons';

import hexToRGBA from '../common-inspector-controls/hex-to-rgba';
import AdvancedColorControl from '../common-inspector-controls/advanced-color-control-palette';
/**
 * Internal block libraries
 */
const { __, sprintf } = wp.i18n;

const { 
	addFilter
} = wp.hooks;

const { 
	createHigherOrderComponent 
} = wp.compose;

const { 
	Component,
	Fragment 
} = wp.element;

const { 
	InspectorControls,
	MediaUpload, 
} = wp.blockEditor;

const { 
	PanelRow,
	PanelBody, 
	ToggleControl,
	Button,
	ButtonGroup,
	Dashicon
} = wp.components;

// Enable spacing control on the following blocks
const enableUkBackgroundOnBlocks = [
	'uk-gb/uk-section-block',
];

// Available spacing control options
const ukGradientOptions = [
	{ label: 'top', value: 'to bottom', icon:<Dashicon icon="arrow-down-alt" /> },
	{ label: 'left', value: 'to right', icon:<Dashicon icon="arrow-right-alt" /> },
	{ label: 'top-left', value: '135deg', icon:<Dashicon icon="arrow-right-alt" /> },
	{ label: 'top-right', value: '305deg', icon:<Dashicon icon="arrow-right-alt" /> },
];

/**
 * Add spacing control attribute to block.
 *
 * @param {object} settings Current block settings.
 * @param {string} name Name of block.
 *
 * @returns {object} Modified block settings.
 */
const addUkBackgroundAttribute = ( settings, name ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkBackgroundOnBlocks.includes( name ) ) {
		return settings;
	}

	// Use Lodash's assign to gracefully handle if attributes are undefined
	settings.attributes = Object.assign( settings.attributes, {

		has_parallax: {
			type: 'bool',
			default: false
		},
		background_image: {
			type: 'string',
			default: null
		},
		backgroundColor_1: {
			type: 'string',
			default: null
		},
		backgroundColor_1_opacity: {
			type: 'number',
			default: '1'
		},
		backgroundColor_2: {
			type: 'string',
			default: null
		},
		backgroundColor_2_opacity: {
			type: 'number',
			default: '1'
		},
		gradientDirection: {
			type: 'string',
			default: 'to bottom'
		},
		bg_final: {
			type: 'string',
			default: null
		}
	} );

	return settings;
};

addFilter( 'blocks.registerBlockType', 'uk-gb/attribute/uk-background', addUkBackgroundAttribute );

/**
 * Create HOC to add UK width to inspector controls of block.
 */
const withUkBackground = createHigherOrderComponent( ( BlockEdit ) => {
	return ( props ) => {
		// Do nothing if it's another block than our defined ones.
		if ( ! enableUkBackgroundOnBlocks.includes( props.name ) ) {
			return (
				<BlockEdit { ...props } />
			);
		}

		const { 

			has_parallax,
			background_image,
			backgroundColor_1,
			backgroundColor_1_opacity,
			backgroundColor_2,
			backgroundColor_2_opacity,
			gradientDirection,
			bg_final

		} = props.attributes;

		if ( props.attributes.backgroundColor_1 ){

			var bg_1 = hexToRGBA( props.attributes.backgroundColor_1, props.attributes.backgroundColor_1_opacity )

			if ( props.attributes.backgroundColor_2 ) {

				var bg_2 = hexToRGBA( props.attributes.backgroundColor_2, props.attributes.backgroundColor_2_opacity )

		   		props.setAttributes( {
                    bg_final: 'linear-gradient('+gradientDirection+','+bg_1+','+bg_2+')',
                } );

			} else {

				props.setAttributes( {
                    bg_final: bg_1,
                } );

			}
				
		} else {

			props.setAttributes( {
                backgroundColor_1: null,
				backgroundColor_1_opacity: '1',
				backgroundColor_2: null,
				backgroundColor_2_opacity: '1',
				gradientDirection: 'to bottom',
				bg_final: null,
            } );

		} 

		return (
			<Fragment>
				<BlockEdit { ...props } />
				<InspectorControls>
					<PanelBody
						title={ __( 'UI Kit Background' ) }
						initialOpen={ true }
					>
					{ ! props.attributes.background_image ? (
                            <MediaUpload
                                onSelect={ ( image ) => {
                                    props.setAttributes( {
                                        background_image: image.url,
                                    } );
                                } }
                                allowedTypes={ [ 'image' ] }
                                value={ props.attributes.background_image }
                                render={ ( { open } ) => (
                                    <Button onClick={ open } isPrimary>
                                        { __( 'Select image' ) }
                                    </Button>
                                ) }
                            />
                        ) : '' }
                        { props.attributes.background_image  ? (
                            <Fragment>
                                <img src={props.attributes.background_image}></img>
                                <p>
	                                <a
	                                    href="#"
	                                    onClick={ ( e ) => {
	                                        props.setAttributes( {
	                                            background_image: '',
	                                        } );
	                                        e.preventDefault();
	                                    } }
	                                >
	                                    { __( 'Remove image' ) }
	                                </a>
	                            </p>
                                <MediaUpload
                                    onSelect={ ( image ) => {
                                        props.setAttributes( {
                                            background_image: image.url,
                                        } );
                                    } }
                                    allowedTypes={ [ 'image' ] }
                                    value={ props.attributes.background_image }
                                    render={ ( { open } ) => (
                                        <Button onClick={ open } isPrimary>
                                            { __( 'Change image' ) }
                                        </Button>
                                    ) }
                                />
                            </Fragment>
                        ) : '' }
                        { props.attributes.background_image  ? (
                            <PanelRow>
								{/* Toggle Switch */}
								<ToggleControl
									label={__("Add Parallax effect to image")}
									checked={!!props.attributes.has_parallax}
									onChange={() => {
										props.setAttributes({ has_parallax: !has_parallax });
									}}
								/>
							</PanelRow>
                        ) : '' }
                        <PanelRow>
                	      	<ButtonGroup
                				className="uk-gb-button-group uk-gb-gradient-buttons"
                	      	>
                				{ map( ukGradientOptions, ( { label, value, icon } ) => (
                					<Button
                						key={ label }
                						className={label}
                						isDefault
                						isPrimary={ props.attributes.gradientDirection === value }
                	                    isToggled={ props.attributes.gradientDirection === value }
                						onClick={ () => {
											
											props.setAttributes({ gradientDirection: value });


                						}
												
                						}
                					>
                						{ icon }
                					</Button>
                				) ) }
                	      	</ButtonGroup>
	                        <AdvancedColorControl
	        					label={ __( 'Color 1' ) }
	        					opacityValue={'1'}
	        					colorValue={ ( props.attributes.backgroundColor_1 ? props.attributes.backgroundColor_1 : '' ) }
	        					colorDefault={ '' }
	        					onColorChange={ ( value ) => {

										props.setAttributes( {
										    backgroundColor_1: value,
										} );

	        						}
									
	        					}
	        					onOpacityChange={ ( value ) => {

										props.setAttributes( {
	    								    backgroundColor_1_opacity: value,
	    								} );
	        						}
	    								
	        					}
	        				/>
	                        { props.attributes.backgroundColor_1  ? (
	                            <AdvancedColorControl
	            					label={ __( 'Color 2' ) }
	            					opacityValue={'1'}
	            					colorValue={ ( props.attributes.backgroundColor_2 ? props.attributes.backgroundColor_2 : '' ) }
	            					colorDefault={ '' }
	            					onColorChange={ ( value ) => {

											props.setAttributes( {
											    backgroundColor_2: value,
											} );

	            						}
	    								
	            					}
	            					onOpacityChange={ ( value ) => {

											props.setAttributes( {
		    								    backgroundColor_2_opacity: value,
		    								} );

	            						}
		    								
	            					}
	            				/>
	                        ) : '' }
	                    </PanelRow>
                        
					</PanelBody>
				</InspectorControls>
			</Fragment>
		);
	};
}, 'withUkBackground' );

addFilter( 'editor.BlockEdit', 'uk-gb/with-uk-background-control', withUkBackground );

/**
 * Add margin style attribute to save element of block.
 *
 * @param {object} saveElementProps Props of save element.
 * @param {Object} blockType Block type information.
 * @param {Object} attributes Attributes of block.
 *
 * @returns {object} Modified props of save element.
 */
const addUkBackgroundExtraProps = ( saveElementProps, blockType, attributes ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkBackgroundOnBlocks.includes( blockType.name ) ) {
		return saveElementProps;
	}

	Object.assign( saveElementProps, { 
		has_parallax: 				attributes.has_parallax,
		background_image: 			attributes.background_image,
		backgroundColor_1: 			attributes.backgroundColor_1,
		backgroundColor_1_opacity: 	attributes.backgroundColor_1_opacity,
		backgroundColor_2: 			attributes.backgroundColor_2,
		backgroundColor_2_opacity: 	attributes.backgroundColor_2_opacity,
		gradientDirection: 			attributes.gradientDirection,
		bg_final: 					attributes.bg_final

	} );

	return saveElementProps;
};

addFilter( 'blocks.getSaveContent.extraProps', 'uk-gb/get-save-content/extra-props', addUkBackgroundExtraProps );

/**
 * Add background wrapper to block.
 *
 */

const saveElementUkBackground = ( element, blockType, attributes ) => {
	// Do nothing if it's another block than our defined ones.
	if ( ! enableUkBackgroundOnBlocks.includes( blockType.name ) ) {
		return element;
	}
	
	// Return the table block with div wrapper.
	return (
		
		<div 
			className={ `some-test-class uk-position-relative ${attributes.background_image && 'uk-background-cover'} ${attributes.align === 'full' ? 'alignfull' : null}`}
			{...attributes.has_parallax && {'uk-parallax' : 'bgy: -200'}}
			style={attributes.background_image && {backgroundImage: `url(${attributes.background_image})`}}
			>
			{attributes.bg_final && 
				<div className="overlay-color uk-fill-parent"
					style={ {background:attributes.bg_final} }
				>

				</div>
			}

			{element}
		</div>
	);
};

addFilter( 'blocks.getSaveElement', 'uk-gb/get-save-element-uk-background', saveElementUkBackground );
