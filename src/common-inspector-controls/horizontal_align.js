import React from 'react';

import map from 'lodash/map';

//import CommonAttributes from './attributes';

const { __ } = wp.i18n; // Import __() from wp.i18n

const {
	createElement,
	forwardRef,
	setAttributes,
} = wp.element;

// Import Inspector components
const {
	Button,
	ButtonGroup,
	Dashicon
} = wp.components;


export function HorizontalAlignmentButtons( {
	horizontalAlignment,
	items,
	onClick

} ) {

   	return (
      	<ButtonGroup
			className="uk-gb-button-group"
      	>
			{ map( items, ( { id, value, icon } ) => (
				<Button
					key={ id }
					isDefault
					isPrimary={ horizontalAlignment === value}
                    isToggled={ horizontalAlignment === value}
					onClick={ () => onClick( value ) }
				>
					{ icon }
				</Button>
			) ) }
      	</ButtonGroup>
   	);
}

export default forwardRef( HorizontalAlignmentButtons );
  
  