/**
 * BLOCK: kitchen-sink
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
 import map from 'lodash/map';

//  Import CSS.

import './style.scss';
import './editor.scss';

// Import icon
import icon from '../icon';

import icons from '../icons';

import edit from './edit.js';

import classnames from 'classnames';

import Background from '../components/background';

import marginClasses from '../common-functions/margin';
import paddingClasses from '../common-functions/padding';
import boxShadowClasses from '../common-functions/box-shadow';
import heightClasses from '../common-functions/height';

const { __ } = wp.i18n; 
const { registerBlockType } = wp.blocks; 

const {
	Component,
	setAttributes,
} = wp.element;

// Register editor components
const {
	InnerBlocks,
} = wp.blockEditor;

// Import Inspector components
const {
} = wp.components;

/**
 * Register: Kitchen Sink Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'uk-gb/uk-grid-block', {
	title: __( 'UI Kit Grid Block' ), 
	icon, 
	category: 'uk-gb-blocks', 
	keywords: [
		__( 'uk', 'UK' ),
		__( 'grid' ),
	],
	supports: {
		align: true,
		alignWide: true,
		anchor: true,
		customClassName: true,
		className: true,
		html: true,
		inserter: true,
		multiple: true,
		reusable: true
	},
	/**
	 * Attributes
	 *
	 * Sources of values that represent the block data that is going to be managed.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/designers-developers/developers/block-api/block-attributes/
	 */
	attributes: {
		// Declare attributes to save various data in block:
		gridSize: {
			type: 'string',
			default: 'uk-grid-default',
		},
		verticalAlignment: {
			type: 'string',
			default: 'v-align-top',
		},
		horizontalAlignment: {
			type: 'string',
			default: 'h-align-left',
		},
		direction: {
			type: 'string',
			default: 'uk-flex-row',
		},
		matchHeight: {
			type: 'bool',
			default: false,
		},
		masonry: {
			type: 'bool',
			default: false,
		},
		wrapClass: {
			type: 'string',
			default: '',
		},
		wrapClass__editor: {
			type: 'string',
			default: '',
		},
		tab_verticalAlignment: {
			default: 'uk-flex-top',
		},
		tab_horizontalAlignment: {
			type: 'string',
			default: 'uk-flex-left',
		},
		tab_stackAlignment: {
			type: 'string',
			default: 'uk-flex-row',
		},
		tab_wrapClass: {
			type: 'string',
			default: '',
		},
		tab_wrapClass__editor: {
			type: 'string',
			default: '',
		},
		mob_verticalAlignment: {
			default: 'uk-flex-top',
		},
		mob_horizontalAlignment: {
			type: 'string',
			default: 'uk-flex-left',
		},
		mob_stackAlignment: {
			type: 'string',
			default: 'uk-flex-row',
		},
		mob_wrapClass: {
			type: 'string',
			default: '',
		},
		mob_wrapClass__editor: {
			type: 'string',
			default: '',
		},
	},

	/**
	 * The edit function describes the structure of your block in the context of the editor.
	 * This represents what the editor will render when the block is used.
	 *
	 * The "edit" property must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	edit,

	/**
	 * The save function defines the way in which the different attributes should be combined
	 * into the final markup, which is then serialized by Gutenberg into post_content.
	 *
	 * The "save" property must be specified and must be a valid function.
	 *
	 * @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
	 */
	save: function(props) {

		// Attributes for the frontend
		const {
			attributes: {
				verticalAlignment,
				horizontalAlignment,
				direction,
				gridSize,
				matchHeight,
				masonry
			},
		} = props;

		if (props.attributes.gridSize) {

			var gSize = props.attributes.gridSize;

		} else {

			gSize = ''

		}
		
		var ukMarginClasses 	= marginClasses(props.attributes);
		var ukPaddingClasses 	= paddingClasses(props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(props.attributes);

		var ukHeightClasses 	= heightClasses(props.attributes);
		
		var wrapperClasses = [ 
			
			ukHeightClasses,
			props.attributes.direction,
			props.attributes.horizontalAlignment,
			props.attributes.verticalAlignment,
			ukMarginClasses,
			ukPaddingClasses,
			ukBoxShadowClasses,

		];

		var flexClasses = [
			'uk-gb-grid',
			'uk-grid', 
			gSize,
			

		];

		return (

			<div className={`${props.attributes.align === 'full' ? 'alignfull' : 'wp-block'} uk-grid-wrapper ${classnames(wrapperClasses)}`}
				uk-height-match={matchHeight ? 'target: > div > div > .uk-grid-item-inner-wrap' : undefined}
				style={ props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:props.attributes.ukHeightDeskCustom+'px'} : {}}	
				uk-height-viewport={props.attributes.ukHeightDesk === 'uk-min-height-viewport' ? "true": false }
			>
				{props.attributes.bg_final || props.attributes.background_image ? <Background attributes={props.attributes}/> : ''}
				<div 
					class={`${classnames(flexClasses)}`} 
					
					uk-grid={props.attributes.masonry ? 'masonry: true' : ''}>
					<InnerBlocks.Content />
				</div>
		   	</div>
		);
	},
});