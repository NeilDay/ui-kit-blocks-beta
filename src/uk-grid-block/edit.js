/**
 * BLOCK: Kadence Column
 *
 * Registering a basic block with Gutenberg.
 */

import $ from "jquery";

import map from 'lodash/map';

import icons from '../icons';

import classnames from 'classnames';

import HorizontalAlignmentButtons from '../common-inspector-controls/horizontal_align';
import VerticalAlignmentButtons from '../common-inspector-controls/vertical_align';
import StackAlignmentButtons from '../common-inspector-controls/stack_align';

import marginClasses from '../common-functions/margin';
import paddingClasses from '../common-functions/padding';
import boxShadowClasses from '../common-functions/box-shadow';
import heightClasses from '../common-functions/height';

// Allowed Children
const ALLOWED_BLOCKS = [ 'uk-gb/uk-grid-item' ];

const { compose} = wp.compose;
/**
 * Internal block libraries
 */
const { __ } = wp.i18n;

const {
	Component,
	setAttributes,
	Fragment
} = wp.element;

const { 
	InnerBlocks,
	InspectorControls,
} = wp.blockEditor;
const {
	PanelBody,
	PanelRow,
	TabPanel,
	Dashicon,
	SelectControl,
	ToggleControl
	
} = wp.components;

var setGridClasses = function(id, gridSize){

	var r = $.Deferred();

	var grid = $('[data-block="'+id+'"] .block-editor-block-list__layout').first();

	$(grid).removeClass('uk-grid-default uk-grid-large uk-grid-small uk-grid-collapse').addClass('uk-grid'+' '+gridSize);

	return r;

}

var initGrid = function(id){
	
	UIkit.grid('[data-block="'+id+'"] .block-editor-block-list__layout.uk-grid');
	
}

/**
 * Build the edit component
 */
class GridBlockEdit extends Component {

	componentDidMount() {

		setGridClasses(this.props.clientId, this.props.attributes.gridSize).done( initGrid(this.props.clientId) );

	}

	componentDidUpdate(prevProps) {
		setGridClasses(this.props.clientId, this.props.attributes.gridSize).done( initGrid(this.props.clientId) );
	}

	render() {

		const { 

			attributes: {
				has_container,
			},

		} = this.props;
		
				// Setup the attributes.
		const {
			attributes: {
				verticalAlignment,
				horizontalAlignment,
				direction,
				gridSize,
				matchHeight,
				masonry
			},
			isSelected,
			editable,
			className,
			setAttributes
		} = this.props;

		const deskValignItems = [ 
	  		{ id: 1, value: 'v-align-top', icon:icons.alignTop},
	  		{ id: 2, value: 'v-align-middle', icon:icons.alignCenter},
	  		{ id: 3, value: 'v-align-bottom', icon:icons.alignBottom}
	  	];

	  	const deskHalignItems = [ 
	  		{ id: 1, value: 'h-align-left', icon:<Dashicon icon="align-left" />},
  			{ id: 2, value: 'h-align-center', icon:<Dashicon icon="align-center" />},
  			{ id: 3, value: 'h-align-right', icon:<Dashicon icon="align-right" />}
	  	];

	  	const deskStackItems = [ 
	  		{ id: 1, value: 'direction-row', icon:<Dashicon icon="grid-view" /> },
  			{ id: 2, value: 'direction-column', icon:<Dashicon icon="editor-justify" /> },
	  	];

	  	const gridSizeOptions = [ 
	  		{ id: 1, label: 'Small', value: 'uk-grid-small'},
	  		{ id: 2, label: 'Normal', value: 'uk-grid-default'},
	  		{ id: 3, label: 'Large', value: 'uk-grid-large'},
	  		{ id: 3, label: 'Collapse', value: 'uk-grid-collapse'}
	  	];

		/*if (this.props.attributes.horizontalAlignment == 'uk-flex-left' && this.props.attributes.stackAlignment == 'uk-flex-column' ) {

			var vAlign = 'wrap-uk-flex-wrap valign-uk-flex-top';	
			var hAlign = '';

		} else if (this.props.attributes.horizontalAlignment == 'uk-flex-center' && this.props.attributes.stackAlignment == 'uk-flex-column' ) {

			var vAlign = 'wrap-uk-flex-wrap valign-uk-flex-middle';	
			var hAlign = '';

		} else if (this.props.attributes.horizontalAlignment == 'uk-flex-right' && this.props.attributes.stackAlignment == 'uk-flex-column' ) {
			
			var vAlign = 'wrap-uk-flex-wrap valign-uk-flex-bottom';	
			var hAlign = '';

		} else { 

			var vAlign = 'valign-'+this.props.attributes.verticalAlignment;
			var hAlign = 'halign-'+this.props.attributes.horizontalAlignment;

		}*/

		if (this.props.attributes.gridSize) {
			var gSize = 'editor-grid-size-'+this.props.attributes.gridSize;	
		} else {
			gSize = ''
		}

		//var stack = 'stack-'+this.props.attributes.stackAlignment;

		var ukMarginClasses 	= marginClasses(this.props.attributes);
		var ukPaddingClasses 	= paddingClasses(this.props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(this.props.attributes);

		var ukHeightClasses 	= heightClasses(this.props.attributes);

		var flexClasses = [
			'uk-grid-editor', 
			ukMarginClasses,
			ukPaddingClasses,
			ukHeightClasses,
			gSize,
			this.props.attributes.direction,
			this.props.attributes.horizontalAlignment,
			this.props.attributes.verticalAlignment,
			ukBoxShadowClasses,

		];

		return [
			<InspectorControls>
				<PanelBody>

					<TabPanel className="uk-gb-tab-panel uk-gb-responsive-settings"
						activeClass="active-tab"
						tabs={ [
							{
								name: 'desktop',
								title: <Dashicon icon="desktop" />,
								className: 'uk-gb-tab uk-gb-desktop-tab',
							},
							{/*{
								name: 'tablet',
								title: <Dashicon icon="tablet" />,
								className: 'uk-gb-tab uk-gb-tablet-tab',
							},
							{
								name: 'mobile',
								title: <Dashicon icon="smartphone" />,
								className: 'uk-gb-tab uk-gb-mobile-tab',
							},*/}
						] }>
						{
							( tab ) => {
								let tabout;
								if ( tab.name ) {
									if ( 'desktop' === tab.name ) {
										tabout = (	
											<Fragment>
												<PanelRow>
													<h2>Gutter</h2>
													<div>
													<SelectControl
															options={gridSizeOptions}
															value={this.props.attributes.gridSize}
															onChange={value => this.props.setAttributes({ gridSize: value })}
														/>
													</div>
												</PanelRow>
												<PanelRow>
													<h2>Vertical Alignment</h2>
													<div>
														<VerticalAlignmentButtons
															verticalAlignment={this.props.attributes.verticalAlignment}
															items={deskValignItems}
															onClick={ ( value ) => this.props.setAttributes({ verticalAlignment: value })}
														/>
													</div>
													
												</PanelRow>
												<PanelRow>
													<h2>Horizontal Alignment</h2>
													<div>
														<HorizontalAlignmentButtons
															horizontalAlignment={this.props.attributes.horizontalAlignment}
															items={deskHalignItems}
															onClick={ ( value ) => this.props.setAttributes({ horizontalAlignment: value })}
														/>
													</div>
												</PanelRow>
												<PanelRow>
													<h2>Cols or Rows</h2>
													<div>
														<StackAlignmentButtons
															stackAlignment={this.props.attributes.direction}
															items={deskStackItems}
															onClick={ ( value ) => this.props.setAttributes({ direction: value })}
														/>
													</div>
												</PanelRow>
												<PanelRow>
													<h2>Match Grid Item Heights</h2>
													<div>
														<ToggleControl
															label={__("")}
															checked={ !! this.props.attributes.matchHeight}
															onChange={() => {
																this.props.setAttributes({ matchHeight: !matchHeight });
															}}
														/>
													</div>
												</PanelRow>
												<PanelRow>
													<h2>Masonry Grid</h2>
													<div>
														<ToggleControl
															label={__("")}
															checked={ !! this.props.attributes.masonry}
															onChange={() => {
																this.props.setAttributes({ masonry: !masonry });
															}}
														/>
													</div>
												</PanelRow>
													
											</Fragment>
										);
									} 
								}
								return <div>{ tabout }</div>;
							}
						}
					</TabPanel>
				</PanelBody>
			</InspectorControls>,
			<div class={`uk-gb-block ${classnames(flexClasses)}`}
				style={ this.props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:this.props.attributes.ukHeightDeskCustom+'px'} : {}}	
				>
				{this.props.isSelected  ? 
					<InnerBlocks
						allowedBlocks={ ALLOWED_BLOCKS } 
						renderAppender={ () => (
							<InnerBlocks.ButtonBlockAppender />
						) }
					/> : 
					<InnerBlocks
						allowedBlocks={ ALLOWED_BLOCKS } 
						renderAppender={ false}
					/>
				}				
			</div>

				
		];
	}
}
export default compose( [

] )( GridBlockEdit );
