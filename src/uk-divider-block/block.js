import map from 'lodash/map';

import classnames from 'classnames';


import marginClasses from '../common-functions/margin';
import HorizontalAlignmentButtons from '../common-inspector-controls/horizontal_align';


//  Import CSS.
import './style.scss';
import './editor.scss';

// Import icon
import icon from '../icon';

import icons from '../icons';

const { __ } = wp.i18n; 
const { registerBlockType } = wp.blocks; 

const {
	setAttributes,
	Fragment,
} = wp.element;

const { 
	InspectorControls, 
} = wp.blockEditor;

const {
	ToggleControl,
    PanelBody,
    PanelRow,
    Dashicon
} = wp.components;

registerBlockType( 'uk-gb/uk-divider', {

	title: __( 'UI Kit Divider' ), // Block title.
	icon, 								// Block icon
	category: 'uk-gb-blocks', 				// Block category
	keywords: [ 						// Search by these keywords
		__( 'uk', 'UK', 'uikit' ),
	],
	supports: {
		className: false,
	},

	attributes: {

		icon: {
			type: 'bool',
			default: false
		},
		small: {
			type: 'bool',
			default: false
		},
		horizontalAlignment: {
			type: 'string',
			default: 'uk-text-center'
		}
		
	},

	edit: function( props ) {


		// Setup the attributes.
		const {
			attributes: {
				icon,
				small,
				horizontalAlignment
			},
			isSelected,
			className,
			setAttributes
		} = props;

		var ukMarginClasses = marginClasses(props.attributes);

		const deskHalignItems = [ 
	  		{ id: 1, value: 'uk-text-left', icon:<Dashicon icon="align-left" />},
  			{ id: 2, value: 'uk-text-center', icon:<Dashicon icon="align-center" />},
  			{ id: 3, value: 'uk-text-right', icon:<Dashicon icon="align-right" />}
	  	];
		
		var ukDividerWrapClasses = classnames({
  			[ukMarginClasses] : ukMarginClasses,
  			[props.attributes.horizontalAlignment] : !props.attributes.icon && props.attributes.small
		});

		var ukDividerClasses = classnames({
  			'uk-divider-icon': props.attributes.icon,
  			'uk-divider-small': !props.attributes.icon && props.attributes.small,
		});

		return [
			<InspectorControls>
					<PanelBody
						title={ __( 'UI Kit Divider Options' ) }
						initialOpen={ true }
					>
						<ToggleControl
							label={__("Small Divider")}
							checked={ !! props.attributes.small}
							onChange={() => {
								props.setAttributes({ small: !small });
							}}
						/>
						<ToggleControl
							label={__("Icon Divider")}
							checked={ !! props.attributes.icon}
							onChange={() => {
								props.setAttributes({ icon: !icon });
							}}
						/>
						<h2>Horizontal Alignment</h2>
						<div>
							<HorizontalAlignmentButtons
								horizontalAlignment={props.attributes.horizontalAlignment}
								items={deskHalignItems}
								onClick={ ( value ) => props.setAttributes({ horizontalAlignment: value })}
							/>
						</div>
					</PanelBody>
				</InspectorControls>,
			<Fragment>
				<div class={ukDividerWrapClasses}>
					<hr class={ukDividerClasses}>
						
					</hr>	
				</div>
					
			</Fragment>
			
		];
	},

	save: function( props ) {

		const {
			attributes: {
				icon,
				small,
				horizontalAlignment
			},
		} = props;

		var ukMarginClasses = marginClasses(props.attributes);

		const deskHalignItems = [ 
	  		{ id: 1, value: 'uk-text-left', icon:<Dashicon icon="align-left" />},
  			{ id: 2, value: 'uk-text-center', icon:<Dashicon icon="align-center" />},
  			{ id: 3, value: 'uk-text-right', icon:<Dashicon icon="align-right" />}
	  	];
		
		var ukDividerWrapClasses = classnames({
  			[ukMarginClasses] : ukMarginClasses,
  			[props.attributes.horizontalAlignment] : !props.attributes.icon && props.attributes.small
		});

		var ukDividerClasses = classnames({
  			'uk-divider-icon': props.attributes.icon,
  			'uk-divider-small': !props.attributes.icon && props.attributes.small,
		});

		return (
			<div class={ukDividerWrapClasses}>
				<hr class={ukDividerClasses}>
					
				</hr>	
			</div>

		);

	},

} );
