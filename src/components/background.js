import React from 'react';

import map from 'lodash/map';

import icons from '../icons';

import classnames from 'classnames';

const { __ } = wp.i18n; // Import __() from wp.i18n

const {
	createElement,
	forwardRef,
	setAttributes,
} = wp.element;

// Import Inspector components
const {
} = wp.components;

export function Background( {

	attributes,

} ) {
	
	//Inner absolutely positioned div for colour/gradient overlay
	const InnerBG = () => {

		var innerBG = false

		if (attributes.bg_final) {

			return (
				
				<div className="overlay-color uk-fill-parent"
					style={ {background:attributes.bg_final} }
				>

				</div>
					
				
				
			)

		} else {
			return null
		}
	}
	
	//Empty style
	var style = {};

	//If we have image add it to style
	if (attributes.background_image) {

		style = {backgroundImage: `url(${attributes.background_image})`}
	}
	

	return (
			
   		<div 
   			className={ `uk-item-background-conatiner uk-background-cover`}
   			{...attributes.has_parallax && {'uk-parallax' : 'bgy: -50'}}
   			style={style}
   			>
   			<InnerBG/>	
   		</div>

   	);

	   	
}

export default forwardRef( Background );