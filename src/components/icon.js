import React from 'react';

import map from 'lodash/map';

import icons from '../icons';

import classnames from 'classnames';

const { __ } = wp.i18n; // Import __() from wp.i18n

const {
	createElement,
	forwardRef,
	setAttributes,
} = wp.element;

// Import Inspector components
const {
} = wp.components;

export function UkIcon( {
	selectedIcon,
	icon,
	onClick

} ) {
	
	const onClickValue = ( event ) => {

		onClick( icon );
		
	};

	const wrapperClasses = classnames( 'uk-gb-icon-holder');

	const iconClasses = classnames( ' uk-icon uk-icon-button uk-gb-icon uk-gb-icon-button',{
		'uk-gb-icon-selected': (selectedIcon === icon),
	} );


	return (
		
		<span className={wrapperClasses}>
			<span 
				className={iconClasses}
				uk-icon={'icon: '+icon}
				onClick={onClickValue}
			></span>
		</span>

   	);

	   	
}

export default forwardRef( UkIcon );