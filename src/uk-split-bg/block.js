import map from 'lodash/map';

import classNames from 'classnames';

import Background from '../components/background';

import icons from '../icons';

import paddingClasses from '../common-functions/padding';
import marginClasses from '../common-functions/margin';
import boxShadowClasses from '../common-functions/box-shadow';
import VerticalAlignmentButtons from '../common-inspector-controls/vertical_align';
import heightClasses from '../common-functions/height';

//  Import CSS.
import './style.scss';
import './editor.scss';

// Import icon
import icon from '../icon';

const { __ } = wp.i18n;

const { registerBlockType } = wp.blocks;

const {
	setAttributes, 
} = wp.element;

const { 
	InspectorControls, 
	InnerBlocks,

} = wp.blockEditor;

const { 
    PanelBody,
    ToggleControl
} = wp.components;


const TEMPLATE = [
	['uk-gb/uk-split-item'],
	['uk-gb/uk-split-item'],
];

const ALLOWED_BLOCKS = [ 'uk-gb/no-blovk-here' ];


const deskValignItems = [ 
	{ id: 1, value: 'uk-flex-stretch', icon:icons.alignStretch},
	{ id: 1, value: 'uk-flex-top', icon:icons.alignTop},
	{ id: 2, value: 'uk-flex-middle', icon:icons.alignCenter},
	{ id: 3, value: 'uk-flex-bottom', icon:icons.alignBottom}
];

registerBlockType( 'uk-gb/uk-split-bg', {
	
	title: __( 'UI Kit Split Background' ), 
	icon, 
	category: 'uk-gb-blocks', 
	keywords: [
		__( 'uk', 'UK' ),
		__( 'container' ),
	],
	supports: {
		align: [ 'full' ],
		anchor: false,
		customClassName: true,
		className: true,
		html: true,
		inserter: true,
		multiple: true,
		reusable: true
	},
    
	attributes: {// Atts required for bg
		
		verticalAlignment: {
			type: 'string',
			default: 'uk-flex-top'
		},
		stackTablet: {
			type: 'bool',
			default: false
		},
		stackMobile: {
			type: 'bool',
			default: true
		},

	},
	
	edit: function( props ) {	

		var ukPaddingClasses = paddingClasses(props.attributes);
		var ukMarginClasses = marginClasses(props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(props.attributes);
		var ukHeightClasses 	= heightClasses(props.attributes);

		const stackClasses = classNames(
  			'uk-gb-block',
  			'uk-split-block',
  			{
    			'stackTablet': props.attributes.stackTablet,
    			'stackMobile': props.attributes.stackMobile
  			}
  			
		);

		return [
			<InspectorControls>
				<PanelBody
					title={ __( 'Content Alignment' ) }
					initialOpen={ true }
				>
					<div className="uk-gb-control-section uk-gb-toggle-control">
						<VerticalAlignmentButtons
							verticalAlignment={props.attributes.verticalAlignment}
							items={deskValignItems}
							onClick={ ( value ) => props.setAttributes({ verticalAlignment: value })}
						/>
					</div>
					<div className="uk-gb-control-section uk-gb-toggle-control">
						<ToggleControl
							label={__("Stack on Tablets")}
							checked={!!props.attributes.stackTablet}
							onChange={() => {
								props.setAttributes({ stackTablet: !props.attributes.stackTablet });
							}}
						/>
					</div>
					<div className="uk-gb-control-section uk-gb-toggle-control">
						<ToggleControl
							label={__("Stack on Mobiles")}
							checked={!!props.attributes.stackMobile}
							onChange={() => {
								props.setAttributes({ stackMobile: !props.attributes.stackMobile });
							}}
						/>
					</div>
				</PanelBody>
			</InspectorControls>,
			<div className={`${stackClasses} valign-${props.attributes.verticalAlignment} ${ukHeightClasses && ukHeightClasses} ${ukPaddingClasses && ukPaddingClasses} ${ukMarginClasses && ukMarginClasses}  ${ukBoxShadowClasses && ukBoxShadowClasses}`}
				style={ props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:props.attributes.ukHeightDeskCustom+'px'} : {}}
				>
				{props.attributes.bg_final || props.attributes.background_image ? <Background attributes={props.attributes}/> : ''}
				{props.isSelected  ? 
					<InnerBlocks 
						allowedBlocks={ ALLOWED_BLOCKS }
						template={TEMPLATE} 
						templateLock={'all'}
						renderAppender={ () => (
							<InnerBlocks.ButtonBlockAppender />
						) }
					/> : 
					<InnerBlocks
						renderAppender={ false}
					/>
				}
			</div>
		];
	},

	save: function( props ) {
		var ukPaddingClasses = paddingClasses(props.attributes);
		var ukMarginClasses = marginClasses(props.attributes);
		var ukBoxShadowClasses 	= boxShadowClasses(props.attributes);
		var ukHeightClasses 	= heightClasses(props.attributes);

		const stackClasses = classNames(
			'wp-block',
  			'uk-gb-block',
  			'uk-split-block',
  			{
    			'stackTablet': props.attributes.stackTablet,
    			'stackMobile': props.attributes.stackMobile
  			}
  			
		);

		return (
			<div className={`${stackClasses} ${ukHeightClasses && ukHeightClasses} ${ukMarginClasses && ukMarginClasses} ${ukBoxShadowClasses && ukBoxShadowClasses}`}
				style={ props.attributes.ukHeightDesk === 'uk-min-height-custom' ? { minHeight:props.attributes.ukHeightDeskCustom+'px'} : {}}	
				uk-height-viewport={props.attributes.ukHeightDesk === 'uk-min-height-viewport' ? 'true': false }
				>
				{props.attributes.bg_final || props.attributes.background_image ? <Background attributes={props.attributes}/> : ''}
				<div className={`uk-position-relative uk-flex ${props.attributes.verticalAlignment} ${ukPaddingClasses && ukPaddingClasses}`}>
					<InnerBlocks.Content />
				</div>
			</div>
				
		);
	},
} );
