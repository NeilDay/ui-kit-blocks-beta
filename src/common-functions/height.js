import classnames from 'classnames';

export default function heightClasses( attributes ) {

	var ukHeightClasses = [];

	ukHeightClasses = classnames({
		'uk-min-height-auto' : attributes.ukHeightDesk === 'uk-min-height-auto',
		'uk-min-height-small' : attributes.ukHeightDesk === 'uk-min-height-small',
		'uk-min-height-medium' : attributes.ukHeightDesk === 'uk-min-height-medium',
		'uk-min-height-large' : attributes.ukHeightDesk === 'uk-min-height-large',
		'uk-min-height-viewport' : attributes.ukHeightDesk === 'uk-min-height-viewport',
		'uk-min-height-custom' : attributes.ukHeightDesk === 'uk-min-height-custom',
	})


	return ukHeightClasses;
}
