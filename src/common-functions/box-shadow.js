import classnames from 'classnames';

export default function boxShadowClasses( attributes ) {

	var ukBoxShadowClasses = [];

	var boxShadowAttributes = [
		'ukBoxShadow',
	] 

	const hasBoxShadowAttribute = boxShadowAttributes.some(attributeName => attributes[attributeName]);

	// If we have bg atts - add the bg + width
	if (hasBoxShadowAttribute) {
		// Width classes
		ukBoxShadowClasses = classnames({
			[attributes.ukBoxShadow] : attributes.ukBoxShadow,
		})

	} else {
		
		ukBoxShadowClasses = '';

	}

	return ukBoxShadowClasses;
	
}