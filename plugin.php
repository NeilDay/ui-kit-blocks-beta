<?php
/**
 * Plugin Name: UI Kit Gutenberg Blocks
 * Plugin URI: https://bang-media.com
 * Description: A set of Gutenberg Blocks using UI Kit.
 * Author: Neil Day
 * Author URI: https://bang-media.com
 * Version: 2.1.3
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 * BitBucket Plugin URI: https://NeilDay@bitbucket.org/NeilDay/ui-kit-guten-blocks.git
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
